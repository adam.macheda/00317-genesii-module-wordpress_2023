<?php
/**
 * Plugin Name:     {wordpress_module_name}
 * Plugin URI:      https://www.genesii.fr
 * Description:     {wordpress_module_description}
 * Author:          Genesii
 * Author URI:      https://www.genesii.fr
 * Text Domain:     genesii
 * Domain Path:     /translations
 * Version:         {wordpress_module_version}
 */

$path = plugin_dir_path(__FILE__);

require_once($path . 'vendor/autoload.php');

use Genesii\Kernel\Plugin;

new Plugin($path);